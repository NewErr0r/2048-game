# Learning by doing - [deusops](https://deusops.com/classbook)

**Личные заметки в целях обучения,а не призыв к действию:**

<details>
  <summary>Light</summary>

### Вариант реализации:

- *Выполняем шаги с 1 по 4 вручную, для наглядного понимания, что необходимо автоматизировать далее, на базе виртуальной машины в Яндекс Облаке, используя следующий образ:*

![img](img/1.png)

- Обновляем списки пакетов и устанавливаем пакеты необходимые для работы:

```bash
apt-get update && apt-get install -y vim-console nginx npm git
```

- Переходим в директорию /opt и выполняем клонирование репозитория с игрой:

```bash
cd /opt
```

```bash
git clone https://gitfront.io/r/deusops/JnacRhR4iD8q/2048-game.git
```

Результат:

![img](img/2.png)

- Переходим в склонированный репозиторий и выполняем установка пакетов и всех зависимостей через **npm**:

```bash
cd 2048-game/
```

```bash
npm install --include=dev
```

Результат:

![img](img/3.png)

- Выполняем сборку проекта:

```bash
npm run build
```

Результат:

![img](img/4.png)

- Выполняем тестовый запуск приложения:

```bash
npm start
```

Результат:

![img](img/5.png)

- Проверяем доступновть приложения в браузере по **http<IP_or_DOMAINNAME>:8080**:

![img](img/6.png)

- Останавливаем приложение, переходим к описанию **system unit** отвечающего за запуск сервиса с игрой:

```bash
vim /etc/systemd/system/game01.service
```

Содержимое:

![img](img/7.png)

- Перечитываем конфигурацию юнитов:

```bash
systemctl daemon-reload
```

- Включаем и добавляем в автозагрузку службу **game01**:

```bash
systemctl enable --now game01
```

Проверяем:

![img](img/8.png)

- Описываем конфигурационный файл для **proxy** используя **nginx**:

```bash
vim /etc/nginx/sites-available.d/game01_proxy.conf
```

Содержимое:

![img](img/9.png)

- Генерируем самоподписанный сертификат для приложения:
    - создаём директорию **/etc/nginx/ssl**:

```bash
mkdir /etc/nginx/ssl
```

- Создаём свой CA:

```bash
openssl req -x509 -sha256 -days 365 -newkey rsa:2048 -keyout ca.key -out ca.crt
```

Результат:

![img](img/10.png)

- Создаём закрытый ключ **game01.key**:

```bash
openssl genrsa -out /etc/nginx/ssl/game01.key 4096
```

- Создаём запрос на подпись:

```bash
openssl req -key /etc/nginx/ssl/game01.key -new -out game01.csr
```

Результат:

![img](img/11.png)

- создаём файл game01.ext с дополнительными параметрами для выпуска сертификата:

```bash
cat <<EOF > game01.ext
  authorityKeyIdentifier=keyid,issuer
  basicConstraints=CA:FALSE
  subjectAltName=@alt_names
  [alt_names]
  DNS.1=game01.company.prof
  IP.1=158.160.160.158
EOF
```

- Выпускаем сертификат **game01.crt**:

```bash
openssl x509 -req -CA ca.crt -CAkey ca.key -in game01.csr -out /etc/nginx/ssl/game01.crt -days 365 -CAcreateserial -extfile game01.ext
```

Результат:

![img](img/12.png)

- Добавляем символьную ссылку из **sites-available.d** на **sites-enabled.d** для конфигурационного файла **game01_proxy.conf:**

```bash
ln -s /etc/nginx/sites-available.d/game01_proxy.conf /etc/nginx/sites-enabled.d/
```

- Проверяем корректность описания конфигурационного файла:

![img](img/13.png)

- Включаем и добавляем в автозагрузку службу **nginx**:

```bash
systemctl enable --now nginx
```

- Проверяем доступность приложения в браузере по **https://<IP_or_DOMAIN_NAME>**:

![img](img/14.png)

### Добавляем простейшую автоматизацию для настройки сервера с использованием ansible:

*подразумевается, что ansible установлен на localhost и все последующие дествия выполняются из под обычного пользователя в директории созданной под данный проект*

*в данном случае "простая автоматизация" - подразумевает из себя использование файлов, такими какие они есть, без использования большого количества переменных и файлов шаблонов, т.е. написание простого playbook-сценария для настройки **конкретного сервера** (на базе Альт 10);*

- Создаём инвернтарный файл **inventory**:

![img](img/15.png)

- Описываем конфигурационный файл **ansible.cfg**:

![img](img/16.png)

- Проверяем связность с удалённов ВМ:

```bash
ansible -m ping game01
```

Результат:

![img](img/17.png)

- Создаём директорию под файлы необходимые для поекта (system unit, proxy.conf, certificates):

```bash
mkdir files
```

В директории **files** создаём и описываем конфигурационный файлы необходимые для приложения:

- **files/game2048.service:**

![img](img/18.png)

- **files/game2048_proxy.conf:**

![img](img/19.png)

- также помещаем **файлы сертификатов** в директорию **files/**:

![img](img/20.png)

- Описываем playbook-сценарий, который будет выполнять автоматизацию для настройки сервера:

```bash
vim playbook.yml
```

Содержимое:

```
---
- name: "Configuring the server for deployment 2048-game"
  hosts: all
  become: true

  vars:
    git_project: https://gitfront.io/r/deusops/JnacRhR4iD8q/2048-game.git

  tasks:
    - name: "Installing the necessary packages"
      community.general.apt_rpm:
        name:
          - "nginx"
          - "npm"
          - "git"
        state: "present"
        update_cache: true

    - name: "Clones the repository with the application {{ git_project }}"
      ansible.builtin.git:
        repo: "{{ git_project }}"
        dest: /opt/2048-game

    - name: "Installing packages and all dependencies via npm"
      ansible.builtin.command:
        cmd: "npm install --include=dev"
        chdir: /opt/2048-game
        creates: /opt/2048-game/node_modules

    - name: "Building the project"
      ansible.builtin.command:
        cmd: "npm run build"
        chdir: /opt/2048-game

    - name: "Copy the system unit file to run the application"
      ansible.builtin.copy:
        src: "files/game2048.service"
        dest: /etc/systemd/system/game2048.service
      notify:
        - "Start and enable game2048"

    - name: "Creating a directory for certificates /etc/nginx/ssl"
      ansible.builtin.file:
        path: /etc/nginx/ssl
        state: "directory"

    - name: "Copying the certificate files"
      ansible.builtin.copy:
        src: "files/{{ item }}"
        dest: /etc/nginx/ssl
      with_items:
        - "game01.crt"
        - "game01.key"

    - name: "Copying the game2048_proxy.conf file"
      ansible.builtin.copy:
        src: "files/game2048_proxy.conf"
        dest: "/etc/nginx/sites-enabled.d/game2048_proxy.conf"
      notify:
        - "Star or restart nginx"

  handlers:
    - name: "Start and enable game2048"
      ansible.builtin.systemd:
        name: "game2048.service"
        state: "started"
        enabled: true

    - name: "Star or restart nginx"
      ansible.builtin.systemd:
        name: "nginx"
        state: "restarted"
        enabled: true
```

- Устанавливаем необходимые **коллекции ansible**:

```bash
ansible-galaxy collection install community.general
```

- Таким образом, структура файлов для данного проекта получается следующая:

![img](img/21.png)

- Выполняем запуск **playbook-сценария**:

```bash
ansible-playbook playbook.yml
```

Результат:

![img](img/22.png)

- Проверяем доступность приложения в браузере по **https://<IP_or_DOMAIN_NAME>**:

![img](img/23.png)

</details>

<details>
  <summary>Normal</summary>

- *на базе виртуальных машины в Яндекс Облаке, используя следующий образ:*

![img](img/1.png)

- Обновляем списки пакетов и устанавливаем пакеты необходимые для работы:

```bash
apt-get update && apt-get install -y vim-console docker-ce git
```

- Запускаем и добавляем в автозагрузку службу **docker**:

```bash
systemctl enable --now docker.service
```

- Переходим в директорию **/opt** и выполняем клонирование репозитория с игрой:

```bash
cd /opt
```

```bash
git clone https://gitfront.io/r/deusops/JnacRhR4iD8q/2048-game.git
```

- Переходим в склонированный репозиторий и описываем простой **Dockerfile** (пока без мультистейджинга, для наглядной разницы в размере образов):

```bash
cd 2048-game/
```

```bash
vim Dockerfile
```

Содержимое:
- **FROM** - в качестве базового образа используем **node:16-alpine**;
- **WORKDIR** - в качестве рабочей директории задаём **/2048-game**;
- **COPY** - копируем файлы **packege.json** и **package-lock.json** в образ:
    - **package.json**  - файл, который служит манифестом проекта и содержит метаданные;
    - **package-lock.json** — это файл, который обеспечивает управление зависимостями и согласованность версий
- **RUN** - выполняем внутри образа установку необходимых пакетов и зависимостей;
- **COPY** - копируем все файлы необходимые для запуска приложения;
- **RUN** - выполняем внутри образа сборку рабочей версии приложения;
- **EXPOSE** - объявляем что вриложение внутри контейнера будет доступно по порту **8080**;
- **CMD** - объявляем что при запуске контейнера будет происходить

![img](img/24.png)

- Выполняем сборку данного образа с тегом **v1**:

```bash
docker build -t 2048-game:v1 .
```

Результат:
- запоминаем что размер получившегося образа - **166МВ**

![img](img/25.png)

- Теперь нужно сделать образ легковесным через мультистейджинг, т.к. результатом через **npm** - является директория **dist** с файлом **index.html**, который в свою очередь ссылается на **bundle.js**

```bash
vim Dockerfile
```

Содержимое:

![img](img/26.png)

- Также опишем файл **.dockerignore** - что бы не тянуть в образ с приложением не нужные для его работы файлы:

```bash
vim .dockerignore
```

Содержимое:

![img](img/27.png)

- Выполняем сборку данного образа с тегом **v2**:

```bash
docker build -t 2048-game:v2 .
```

Результат:
- наблюдаем что образ стал практически в 3 раза легче:

![img](img/28.png)

- Запускаем контейнеры с **v1** и **v2** для проверки функционала:

```bash
docker run --name 2048-game-big-size -d -p 8001:8080 2048-game:v1
```

```bash
docker run --name 2048-game-lite-size -d -p 8002:80 2048-game:v2
```

Результат:

![img](img/29.png)

Проверяем функционал:
- тяжелый образ - через порт **8001**:

![img](img/30.png)

- лёгкий образ - через порт **8002**:

![img](img/31.png)

*Таким образом, функционал не пострадал, а значит получилось сделать образ легковесным через мультистейджинг*

- Перед написанием **gitlab-ci** пайплайна для автоматической сборки и публикации Docker-образа с приложением удалим имеющийся каталог **.git**:

```bash
rm -rf .git
```

- В **gitlab** создадим репозиторий под данный проект с именем **2048-game** и добавим все файлы исходного приложения и написанный **Dockerfile**:

Результат:

![img](img/32.png)

### Запускаем свой gitlab-runner и добавляем его в gitlab:

*Установка и запуск gitlab-runner производится на отдельной ВМ на базе Альт*

- Выполним установку **curl**:

```bash
apt-get update && apt-get install -y curl
```

- Скачиваем бинарный файл для **gitlab-runner**:

```bash
curl -L --output /usr/local/bin/gitlab-runner "https://s3.dualstack.us-east-1.amazonaws.com/gitlab-runner-downloads/latest/binaries/gitlab-runner-linux-amd64"
```

- Задаём права на исполнение:

```bash
chmod +x /usr/local/bin/gitlab-runner
```

- Создаём пользователя **gitlab-runner**:

```bash
useradd --comment 'GitLab Runner' --create-home gitlab-runner --shell /bin/bash
```

- Устанавливаем сервис **gitlab-runner**:

```bash
gitlab-runner install --user=gitlab-runner --working-directory=/home/gitlab-runner
```

- Запускаем сервис **gitlab-runner**:

```bash
gitlab-runner start
```

- Устанавливаем **docker**, т.к. планируется использовать **dind (docker-in-docker)**:

```bash
apt-get install docker-ce -y
```

- Запускаем службу **docker**:

```bash
systemctl enable --now docker.service
```

- Добавляем пользователя **gitlab-runner** в группу **docker**:

```bash
usermod -aG docker gitlab-runner
```

- Выполняем регистрацию **gitlab-runner** в **gitlab**:

```bash
gitlab-runner register
```

Результат:
1. Указываем URL-адрес экземпляра GitLab;
2. Указываем регистрационный токен (далее показано где его взять);
3. Указываем комментрий для ранера;
4. Указываем docker в качестве executer;
5. Указываем default образ, который будет использоваться по умолчанию, если не указано будет иное

![img](img/33.png)

*Регистрционный токен можно найти в настройках репозитория:*

![img](img/34.png)

- Проверяем доступность **gitlab-runner** в **gitlab**:

![img](img/35.png)

- Вносим дополнительные настройки в конфигурационный файл **/etc/gitlab-runner/config.toml** для корректной работы **dind**:

```bash
vim /etc/gitlab-runner/config.toml
```

Вносим следующие изменения:

![img](img/36.png)

- Перезапускаем сервис **gitlab-runner**:

```bash
systemctl restart gitlab-runner
```

- Описываем **gitlab-ci пайплайн** для автоматической сборки и публикации Docker-образа с приложением

```bash
vim .gitlab-ci.yml
```

Содержимое:

```
---

stages:
  - build_and_push

build and push docker:
  image: docker:stable
  services:
    - docker:dind
  stage: build_and_push
  script:
    - docker login -u gitlab-ci-token -p ${CI_JOB_TOKEN} ${CI_REGISTRY}
    - docker build -t ${CI_REGISTRY_IMAGE}:${CI_COMMIT_REF_SLUG} .
    # rule for master-branch
    - if [[ ${CI_COMMIT_BRANCH} == ${CI_DEFAULT_BRANCH} ]]; then
          docker tag ${CI_REGISTRY_IMAGE}:${CI_COMMIT_REF_SLUG} ${CI_REGISTRY_IMAGE}:latest;
          docker push ${CI_REGISTRY_IMAGE}:latest;
      fi
    # rule for branch and tags
    - if [[ -z "{CI_COMMIT_TAG}" ]]; then
          docker push ${CI_REGISTRY_IMAGE}:${CI_COMMIT_REF_SLUG};
      else
          docker tag ${CI_REGISTRY_IMAGE}:${CI_COMMIT_REF_SLUG} ${CI_REGISTRY_IMAGE}:${CI_COMMIT_TAG};
          docker push ${CI_REGISTRY_IMAGE}:${CI_COMMIT_TAG};
      fi
  tags:
    - docker
```

- Выполняем **commit** и **push** в репозиторий для проверки работоспособности **gitlab-ci пайплайна**:

```bash
git add .gitlab-ci.yml
```

```bash
git commit -m "Added gitlab-ci file"
```

```bash
git push -uf origin main
```

- Проверяем пайплайн:

![img](img/37.png)

![img](img/38.png)

![img](img/39.png)

- Переходим в **Container Registry** и проверяем наличие Docker-образа с приложением

![img](img/40.png)

![img](img/41.png)

- Проверяем работоспособнойть пайплайна, если добавить **tag**:

![img](img/42.png)

![img](img/43.png)

![img](img/44.png)

![img](img/45.png)

- Проверяем наличие образа с тегом:

![img](img/46.png)

</details>
